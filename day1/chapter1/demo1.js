// for uotput use console.log

console.log('hi am nihal');
////arthematic

console.log(2-5+8);
// variables
var x;
x=3;  //assignment
console.log('x is :',x);

x=x+5;
console.log('now x is :',x);
let y=x*5;
console.log('y is :',y);

/// data types
var myname='nihal';
var num =13;
var state = false;
var mydetails={
    name:'nihal',
    num:13,
    state:false
}

console.log(typeof(myname));
console.log(typeof(num));
console.log(typeof(state));
console.log(typeof(mydetails));

/* rules of variables 
Names can contain letters, digits, underscores, and dollar signs.
Names must begin with a letter
Names can also begin with $ and _ (but we will not use it in this tutorial)
Names are case sensitive (y and Y are different variables)
Reserved words (like JavaScript keywords) cannot be used as names
The variables lastName and lastname, are two different variables:
*/

const fun =3;
// fun= 4;
console.log('fun is :' , fun);
////
