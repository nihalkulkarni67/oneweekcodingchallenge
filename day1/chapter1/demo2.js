//demo is for data manipultation

var x= 34;
var y= 'hello';
var z= 56;

console.log('left',x+z+y);
console.log('right',y+x+z);
console.log('z is',typeof(z));
z=z.toString();
console.log('z is',z,typeof(z));

///array a special object

var register = ['nihal','dhruv','rati','gowda'];

console.log('array 1 is',register[1]);

register[1]='shiv';
console.log('array 1 is now',register[1]);

console.log(register.length);
//// object manipulation 
var nihal = {
    roll:1,
    class:5,
    marks:48,
    total:50,
    lastname:'kulkarni',
    contact:{
        phone:'60606040',
        email:'nihalkulkarni67@gmail.com'
    },
    extra:null
};

console.log('my lastname is ',nihal.lastname);
console.log('my roll number is ', nihal.roll);
var percentage=nihal.marks/nihal.total*100;
console.log('percentage is ', percentage+'%');

nihal.roll=3;
console.log('new roll',nihal.roll);

var mynumber= nihal.contact.phone;
console.log('my phone number is ', mynumber);

nihal.extra={
    sports:'basketball',
    color:'red'
    };

console.log('extra added info',nihal.extra);
console.log('accesing info color:',nihal.extra.color);


///string manipulation
let f1 = 'how are';
let f2 = 'you'
console.log(f1+f2);

let g = f1.slice(0,3) + ' is he?';  //slice method
console.log('g is: ',g);

let sent ='this is a sentenance';
console.log('one single character in a sentence :',sent[1]);
console.log('lenght of string is ',sent.length);

let newsent ='these are my toy\'s and \n this is another line'; // escape character


console.log('new sentence is ',newsent);
console.log('postion of another in newsent ',newsent.indexOf('another')); //indexOf method , search() can also be used here

console.log('replacing line to sentence',newsent.replace('line','sentence')); //replace
console.log('to upper case',newsent.toUpperCase());

////

// array in object and object in array
let students = [
    {
        name:'nihal',
        class:8,
        marks:[45,67,88,99,88,99]
    },{
        name:'shiv',
        class:9,
        marks:[45,67,88,99,88,99]
    },{
        name:'dhruv',
        class:8,
        marks:[45,67,88,99,88,99]
    },{
        name:'sania',
        class:10,
        marks:[45,67,88,99,88,99]
    }
    
];

console.log('roll 1 student ', students[0]);
console.log('student name',students[0].name);
console.log('first student first subject marks',students[0].marks[0]);
console.log('turning object to string',JSON.stringify(students));

//array methods

let arr1 = [1,2,34,35,6,56,45];
let arr2 = [23,234,35,34,63,6,45,7];

let arr3= arr1.concat(arr2);
console.log('concated array is ',arr3);

// adding new element 
arr1[arr1.length]=79;
console.log('added 79 to array',arr1);
arr1.push(88);
console.log('added 88 to array',arr1); // same thing as arr1[arr1.lenght]

arr1.pop();
console.log('removing last element',arr1);

arr1.shift();
console.log('removes the first index',arr1);

arr1.unshift(45);
console.log('adds to first element',arr1);



