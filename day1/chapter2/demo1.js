// chapter 2 is for conditons

// if else loop

let x= 45;
let y=10;

if(x<90){
    y=55;
    console.log('if statment executed');
}
console.log('y is ', y);


if(x===55){
    console.log('in the 2nd if');
}else{
    console.log('in the 2nd else');
};


if(x>y){
    console('x is greater');
}else if(x<y){
    console.log('y is greter');
}else{
    console.log('both x and y are equal');
};


let f1= 10;
let f2= 20;
let f3=30;
//write conditon if f2 is should be less then f3 but greater than f1
// this can be done in two ways


if(f2<f3){

    if(f2>f1){
        console.log('success using condition nesting');
    }
}

if(f2<f3 && f2>f1){
    console.log('success without nesting');
}



let s1= '1';
let s2=1;

if(s1==s2){
    console.log('s1==s2');
}else{
    console.log('s1!=s2');
}

if(s1===s2){
    console.log('s1===s2');
}else{
    console.log('s1!==s2');
}


//// switch condtion


let today=new Date().getDay();

console.log('today is',today);

switch(today){
    case 0: 
        console.log('today is sunday');
        break;
    case 1:
        console.log('today is monday');
        break;
    case 2:
        console.log('today is tuesday');
        break;
    case 3:
        console.log('today is wednesday');
        break;
    case 4:
        console.log('today is thursday');
        break;
    case 5:
        console.log('today is friday');
        break;
    case 6:
        console.log('today is saturday');
        break;
    default:
        console.log('invalid day');
        break;

}

