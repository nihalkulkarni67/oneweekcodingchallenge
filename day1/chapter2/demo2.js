// loops
let arr =[5,6,7,83,4,34,2,4];
let sent ='hi this is sentence ';

// say hi 5 times

for(let i=0;i<5;i++){
    console.log('hi',i+1);
}
// console.log('i is ',i); //for understanding diff betwm let and var, replace let with var 
//display array

for(let i=0;i<arr.length;i++){
    console.log('item at ',i,'is',arr[i]);
}
var x;
for(x of arr){
    console.log('items are',x);
};

let nihal={
    roll:10,
    class:5,
    hobbies:'electronics',
    ambition:'engineer'
}

let z;
for(z in nihal){
    console.log('z is',z);
    console.log('using z',nihal[z]);
};



/// while and do while

let j=0;
while(j<10){
    console.log('j is ',j);
    j++;
}

let m=0;
do{
    m++;
    console.log('m is ',m);

}while(m<10);


//// break

for(let k=0;k<10;k++){
    console.log('k is',k);
    if(k===3){
        console.log('break');
        break;
    }
}

for(let k=0;k<10;k++){
    if(k===4){
        continue;
    }
    console.log('k in con is',k);
}

////