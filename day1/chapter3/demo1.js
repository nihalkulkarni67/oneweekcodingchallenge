//functions

function add(a,b){
    return a+b;
}

var sum=add(4,5)
console.log('add function results',sum);
///

//local variable 
// let fd = 3;
function testvar(i){
    let fd =i;
    console.log('fd here is ',fd);
}

testvar(2);
// console.log(testvar(2));
// console.log('fd is',fd);

//method in objects

let arr ={
    name:'nihal',
    degree:'b.tech',
    show(){
        return 'i am '+this.name+', i have completed '+this.degree;
    }
}

console.log('testing method',arr.show());