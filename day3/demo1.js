//js classes

// const { func } = require("prop-types");

// const { func } = require("prop-types");

// class car {
//   constructor(name, modelDate, fueltype) {
//     this.name = name; //these are called property or class variables
//     this.modelDate = modelDate;
//     this.fueltype = fueltype;
//   }
//   age() {
//     // this is called method
//     let date = new Date().getFullYear();
//     return date - this.modelDate;
//   }
//   extra(info) {
//     // this is called setter method
//     this.info = info;
//   }
// }

// let mycar = new car("i10", 2010, "petrol");
// let rahulscar = new car("ford", 2019, "diesel");
// console.log("my car fuel type", mycar.fueltype);
// console.log("age of mycar", mycar.age());

// rahulscar.extra("rahul's car has damaged chasis");

// console.log("extra info of rahul's car :", rahulscar.info);
// console.log(typeof rahulscar);

// //settimeout and setimeinterval
// console.log("Below comment will be displayed in 3 sec");
// setTimeout(() => {
//   console.log("3 seconds are over");
// }, 3000);

// var c = 0;
// let timer = setInterval(() => {
//   console.log("c is ", c);
//   if (c === 5) {
//     clearInterval(timer);
//   }
//   c++;
// }, 1000);





let nihal={
  roll:12,
  student:'3n',
  hobbies:'dsda',
  foo:function () {
    return 'hi';
  }
}

class student{
  constructor(roll,student,hobbies){
    this.roll=roll;
    this.student=student;
    this.hobbies=hobbies;
  }
  foo(){
    return 'hi'
  }
}

class specialstudent extends student{
  show(){
    return 'bye'
  }
}

let rama =new specialstudent(17,'67','fgdgfdf');
let charuchandra = new student(14,'2b','dancing');
let shiv =new student(22,'7','coding');
console.log(nihal.foo()); // written
console.log(charuchandra.foo()); // created
console.log(shiv.foo());
console.log(rama.show());


// function charu (){
//   console.log('sdfsfdsdf');
// }

// let charu = function(){
  
// };


let r1 = {    //t,r,q
  t:'1212',
  r:'ewrwer',
  q:{
    fg:'sdsd',
    jk:'sds'
  }
};

r1 ={
  q:{
    fg:'fdfsf'
  }
};
console.log('r1 is',r1);